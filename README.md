# Waste Manager

## Contexte :

Afin d'aider le commune de Simplon les Deux Châteaux à améliorer leur gestion des déchets, un logiciel à été concu afin de gérer les déchets générés par les habitants tout en tenant compte des services de traitements des déchets disponibles.

Les types de déchets (non exhaustifs) :
* Déchets gris, le déchet de base
* Déchets plastiques, qui seront de différents types
* Déchets cartons
* Déchets organiques

Les services de traitements des déchets potentiellement disponibles :
* Incinérateur, qui accepte tout type de déchets
* Centre de tri des déchets recyclables ou compostables, se charge de trier les déchets pour les envoyer aux centres de traitements adequat.
* Centre de recyclage, peuvent traiter carton et/ou plastiques, potentiellement selon leur type
* Composteurs de quartier, peuvent traiter les déchets compostables

Les poids des déchets et les capacités de traitement sont en tonne.

**Le systeme de gestion des déchets est concu pour minimiser les émissions de CO2.**

## Contrainte technique :

- Programmation orientée objet.
- Minimisation des émissions de CO2 en favorisant des methodes de traitement à faible émission.

#### Emission de CO2 par service de traitement de déchets :

Ci-dessous des valeurs factices de CO2 rejettés pour les différents déchets via leur mode de traitement, expimés en kg de CO2/tonne de déchet traité :

|                 | Incinération | Recyclage | Compostage |
| --------------- | ------------ | --------- | ---------- |
| Gris (ou autre) | 30           |           |            |
| Plastique PET   | 40           | 8         |            |
| Plastique PVC   | 38           | 12        |            |
| Plastique PC    | 42           | 10        |            |
| Plastique PEHD  | 35           | 11        |            |
| Carton          | 25           | 5         |            |
| Organique       | 28           |           | 1          |
| Métaux          | 50           | 7         |            |
| Verre           | 50           | 6         |            |


