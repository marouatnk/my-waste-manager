<?php

namespace App\Entity;

use App\Repository\WasteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WasteRepository::class)
 */
class Waste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subType;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $CO2emitted;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $destroy;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $processWasteType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSubType(): ?string
    {
        return $this->subType;
    }

    public function setSubType(?string $subType): self
    {
        $this->subType = $subType;

        return $this;
    }

    public function getweight(): ?float
    {
        return $this->weight;
    }

    public function setweight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getCO2emitted(): ?float
    {
        return $this->CO2emitted;
    }

    public function setCO2emitted(?float $CO2emitted): self
    {
        $this->CO2emitted = $CO2emitted;

        return $this;
    }

    public function isDestroy(): ?bool
    {
        return $this->destroy;
    }

    public function setDestroy(): void
    {
        $this->destroy = true;

    }

    public function getProcessWasteType(): ?string
    {
        return $this->processWasteType;
    }

    public function setProcessWasteType(?string $processWasteType): self
    {
        $this->processWasteType = $processWasteType;

        return $this;
    }
}
