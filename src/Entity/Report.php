<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbWastes;

    /**
     * @ORM\Column(type="integer")
     */
    private $sortedWastes;

    /**
     * @ORM\Column(type="float")
     */
    private $totalOfCO2Emitted;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $unsortedWastes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNbWastes(): ?int
    {
        return $this->nbWastes;
    }

    public function setNbWastes($nbWastes): self
    {
        $this->nbWastes = $nbWastes;

        return $this;
    }

    public function getSortedWastes(): ?int
    {
        return $this->sortedWastes;
    }

    public function setSortedWastes(int $sortedWastes): self
    {
        $this->sortedWastes = $sortedWastes;

        return $this;
    }

    public function getTotalOfCO2Emitted(): ?float
    {
        return $this->totalOfCO2Emitted;
    }

    public function setTotalOfCO2Emitted(float $totalOfCO2Emitted): self
    {
        $this->totalOfCO2Emitted = $totalOfCO2Emitted;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUnsortedWastes(): ?int
    {
        return $this->unsortedWastes;
    }

    public function setUnsortedWastes(?int $unsortedWastes): self
    {
        $this->unsortedWastes = $unsortedWastes;

        return $this;
    }
}
