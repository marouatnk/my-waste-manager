<?php

namespace App\Controller;

use App\Business\ReportManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Business\WasteManager;
use App\Form\FileUploaderType;
use App\Repository\ReportRepository;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use Doctrine\ORM\EntityManagerInterface;


class ManagerController extends AbstractController
{
    /**
     * @Route("/manager", name="manager")
     */
    public function send_report(Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(FileUploaderType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entryPoint = $form->getData();
            $entryPoint = json_decode(file_get_contents($entryPoint["file"]), TRUE);

            if ($entryPoint) {
                $wasteManager = new WasteManager(
                    $entryPoint,
                    [
                        $this->getParameter('incinerator_emission'),
                        $this->getParameter('recycling_emission'),
                        $this->getParameter('composter_emission')
                    ]
                );

                $wasteManager->createHandlers();
                $wasteManager->createWastes();
                $wasteManager->sortingWastes();

                $report = new ReportManager();
                $report->create($wasteManager);
                $id = $report->persist($manager);

                return $this->redirectToRoute('show_report', ['id' => $id]);
            }
        }
        return $this->render('upload_file.html.twig', [
            'form' => $form->createView()
        ]);
    }

    // /**
    //  * @Route("/show/{id}", name="show_report")
    //  */
    public function show_report(int $id, ReportManager $report, ReportRepository $reportRepo)
    {

        // [$handlersByWastes,$handlersCO2Issued] = $report->getChartResume($id, $reportRepo);
        
        $handlersByWastesChart = new PieChart();
        $handlersByWastesChart->getOptions()->setTitle('Déchets par type de traitement');
        // $handlersByWastesChart->getData()->setArrayToDataTable($handlersByWastes);

        $handlersCO2IssuedChart = new PieChart();
        $handlersCO2IssuedChart->getOptions()->setTitle('Émission de CO2 par type de traitement');
        // $handlersCO2IssuedChart->getData()->setArrayToDataTable($handlersCO2Issued);


        return $this->render('show_report.html.twig', [
            'report' => $report->get($id, $reportRepo),
            'handlersByWastes' => $handlersByWastesChart,
            'handlersCO2IssuedChart' => $handlersCO2IssuedChart
        ]);
    }

    /**
     * @Route("/all-reports", name="report_list")
     */
    public function list_report(ReportManager $report, ReportRepository $reportRepo)
    {

        return $this->render('list_report.html.twig', [
            'reports' => $report->getAll($reportRepo),
        ]);
    }
}
