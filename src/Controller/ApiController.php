<?php

namespace App\Controller;

use App\Business\Report;
use App\Business\ReportManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Business\WasteManager;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/", name="api_index", methods={"POST"})
     */
    public function send_report(Request $request, EntityManagerInterface $manager)
    {
        $input = array();
        if ($content = $request->getContent()) {
            $input = json_decode($content, true);

            if ($input) {
                $wasteManager = new WasteManager(
                    $input,
                    [
                        $this->getParameter('incinerator_emission'),
                        $this->getParameter('recycling_emission'),
                        $this->getParameter('composter_emission')
                    ]
                );

                $wasteManager->createHandlers();
                $wasteManager->createWastes();
                $wasteManager->sortingWastes();

                $response = array(
                    'dechets' => $wasteManager->getTotalWastes(),
                    'dechetsNonTraites' => ($wasteManager->getTotalWastes() - $wasteManager->getTotalWastesSorted()),
                    'dechetsTraites' => $wasteManager->getTotalWastesSorted(),
                    'totalCO2Emis' => $wasteManager->getTotalCO2emitted(),
                    'date' => new DateTime(),
                );
                $wastes = $wasteManager->getWastes();
                $json = array(
                    'response' => $response,
                    'wastes' => $wastes,
                );
                $report = new ReportManager();
                $report->create($wasteManager);
                $report->persist($manager);

                return new JsonResponse($json);

            } 
        }

        return new JsonResponse(['response' =>
        [
            "state" => "error"
        ]]);
    }
}