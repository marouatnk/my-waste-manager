<?php

namespace App\Business;

use App\Entity\Waste;
use App\Services\ComposterHandler;
use App\Services\IncineratorHandler;
use App\Services\RecyclingCenterHandler;
use App\Services\SortingCenterHandler;
use App\Business\WasteManagerInterface;
use App\Services\Composter;
use App\Services\Incinerator;
use App\Services\Recycle;
use App\Services\SortingCenter;

class WasteManager implements WasteInterface
{
    private array $input;
    private array $incineratorEmission;
    private array $recyclingEmission;
    private array $composterEmission;
    private array $wastes = array();
    private array $sortingCenters = array();
    private array $handlers = array();

    function __construct(array $input, array $emission)
    {
        $this->input = $input;
        $this->incineratorEmission = $emission[0];
        $this->recyclingEmission = $emission[1];
        $this->composterEmission = $emission[2];
    }

    // Création des déchets based on districts
    public function createWastes(): void
    {
        for ($i = 0; $i < count($this->input["quartiers"]); $i++) {

            for ($ii = 1; $ii < count($this->input["quartiers"][$i]); $ii++) {

                if (is_array(array_values($this->input["quartiers"][$i])[$ii])) {
                    for ($iii = 0; $iii < count(array_values($this->input["quartiers"][$i])[$ii]); $iii++) {

                        $mass = array_values(array_values($this->input["quartiers"][$i])[$ii])[$iii];
                        $type = array_keys($this->input["quartiers"][$i])[$ii];
                        $subType = array_keys(array_values($this->input["quartiers"][$i])[$ii])[$iii];

                        array_push($this->wastes, new Waste($mass, $type, $subType));
                    }
                } else {
                    $mass = array_values($this->input["quartiers"][$i])[$ii];
                    $type = array_keys($this->input["quartiers"][$i])[$ii];

                    array_push($this->wastes, new Waste($mass, $type));
                }
            }
        }
    }

    public function getWastes(): array
    {
        $wastes = array();
        for ($i = 0; $i < count($this->wastes); $i++) {

            array_push(
                $wastes,
                array(
                    'CO2Iemitted' => $this->wastes[$i]->getCO2emitted(),
                    'destroyed' => $this->wastes[$i]->isDestroy(),
                    'SortingMethod' => $this->wastes[$i]->getProcessWasteType(),
                    'type' => $this->wastes[$i]->getType(),
                    'subType' => $this->wastes[$i]->getSubType()
                )
            );
        }
        
        return $wastes;
    }

    // Création des services de traitements des déchets.
    public function createHandlers(): void
    {

        for ($i = 0; $i < count($this->input["services"]); $i++) {

            switch ($this->input["services"][$i]["type"]) {
                case "centreTri":

                    $capacityMax = array_values($this->input["services"][$i])[1];
                    array_push($this->sortingCenters, new SortingCenter($capacityMax));
                    break;

                case "incinerateur":

                    $ovenLineMax = array_values($this->input["services"][$i])[1];
                    $capacityLineMax =  array_values($this->input["services"][$i])[2];
                    array_push($this->handlers, new Incinerator($ovenLineMax, $capacityLineMax, $this->incineratorEmission));
                    break;

                case "recyclagePlastique":
                case "recyclagePapier":
                case "recyclageVerre":
                case "recyclageMetaux":

                    $capacityMax = NULL;
                    $type = NULL;
                    $subType = array();
                    $deposit = false;

                    for ($ii = 0; $ii < count($this->input["services"][$i]); $ii++) {
                        if (array_keys($this->input["services"][$i])[$ii] == "capacite") {
                            $capacityMax = array_values($this->input["services"][$i])[$ii];
                        }
                    }
                    for ($ii = 0; $ii < count($this->input["services"][$i]); $ii++) {
                        if (array_keys($this->input["services"][$i])[$ii] == "type") {
                            $type = array_values($this->input["services"][$i])[$ii];
                        }
                    }
                    for ($ii = 0; $ii < count($this->input["services"][$i]); $ii++) {
                        if (array_keys($this->input["services"][$i])[$ii] == "plastiques") {
                            $subType = array_values($this->input["services"][$i])[$ii];
                        }
                    }
                    for ($ii = 0; $ii < count($this->input["services"][$i]); $ii++) {
                        if (array_keys($this->input["services"][$i])[$ii] == "consigne") {
                            $deposit = array_values($this->input["services"][$i])[$ii];
                        }
                    }

                    array_push($this->handlers, new Recycle($capacityMax, $type, $subType, $deposit, $this->recyclingEmission));
                    break;

                case "composteur":

                    $capacityMax = $this->input["services"][$i]["capacite"];
                    $family = $this->input["services"][$i]["foyers"];

                    array_push($this->handlers, new Composter($capacityMax, $family, $this->composterEmission));
                    break;

                default:
                    break;
            }
        }
    }

    // sorting wastes
    public function sortingWastes(): void
    {
        for ($i = 0; $i < count($this->wastes); $i++) {

            for ($ii = 0; $ii < count($this->sortingCenters); $ii++) {

                if ($this->sortingCenters[$ii]->canHandle($this->wastes[$i]) && !$this->wastes[$i]->isDestroy()) {
                    $this->sortingCenters[$ii]->handle($this->wastes[$i], $this->handlers);
                }
            }
        }
        for ($i = 0; $i < count($this->wastes); $i++) {

            for ($ii = 0; $ii < count($this->handlers); $ii++) {

                // if center if full send the rest of the waste to the incinerator 
                if ($this->handlers[$ii] instanceof Incinerator) {

                    if ($this->handlers[$ii]->canHandle($this->wastes[$i]) && !$this->wastes[$i]->isDestroy()) {
                        $this->handlers[$ii]->handle($this->wastes[$i]);
                    }
                }
            }
        }
    }

    public function getTotalWastes(): int
    {
        return count($this->wastes);
    }

    public function getTotalWastesSorted(): int
    {
        $totalTreatedWastes = 0;

        for ($i = 0; $i < count($this->wastes); $i++) {

            if ($this->wastes[$i]->isDestroy()) {
                $totalTreatedWastes++;
            }
        }

        return $totalTreatedWastes;
    }

    public function getTotalCO2emitted(): float
    {
        $totalCO2emitted = 0;

        for ($i = 0; $i < count($this->wastes); $i++) {

            if ($this->wastes[$i]->isDestroy()) {
                $totalCO2emitted += $this->wastes[$i]->getCO2emitted();
            }
        }

        return $totalCO2emitted;
    }
}
