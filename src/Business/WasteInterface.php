<?php
namespace App\Business;

interface WasteInterface
{
    function createWastes() : void;
    function createHandlers() : void;
    function sortingWastes() : void;
    function getTotalWastes() : int;
    function getTotalWastesSorted() : int;
    function getTotalCO2emitted() : float;
    function getWastes() : array;
}