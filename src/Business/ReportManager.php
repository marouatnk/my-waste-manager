<?php

namespace App\Business;

use App\Entity\Report as EReport;
use App\Entity\WasteManager;
use DateTime;

class ReportManager
{
    private $report;

    public function create($wasteManager): void
    {

        $this->report = new EReport();
        $this->report
            ->setDate(new DateTime())
            ->setNbWastes($wasteManager->getWastes())
            // ->setSortedWastes($wasteManager->getWastes() - $wasteManager->getTotalWastesSorted())
            ->setSortedWastes($wasteManager->getTotalWastesSorted())
            ->setTotalOfCO2emitted($wasteManager->getTotalCO2emitted());
    }

    public function persist($manager): int
    {
        $manager->persist($this->report);
        $manager->flush();
        return $this->report->getId();
    }

    public function get(int $id, $repo)
    {
        return $repo->find($id);
    }

    public function getAll($repo)
    {
        return $repo->findAll();
    }
}
