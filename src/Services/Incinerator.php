<?php

namespace App\Services;

use App\Entity\Waste;

class Incinerator implements ServicesInterface {

    private $capacityMax;
    private array $emission;
    private float $CO2emitted = 0;

    function __construct(int $lineMax, int $capacityLineMax)
    {
        $this->capacityMax = $lineMax * $capacityLineMax;
    }

    public function getCO2emitted() : float
    {
        return $this->CO2emitted;
    }

    public function getCO2AfterSorting(Waste $waste): float
    {
        if ($waste->getSubType() !== null) {

            return current(array_column($this->emission[$waste->getType()], current([$waste->getSubType()]))) / 1000 * $waste->getWeight();
        } 
        else {
            return $this->emission[$waste->getType()] / 1000 * $waste->getWeight();
        }
    }

    public function canHandle(Waste $waste) : bool
    {
        if( $this->capacityMax >= $waste->getWeight() )
        {
            return true;
        }
        return false;
    }

    public function handle(Waste $waste) : void 
    {
        $this->capacityMax -= $waste->getWeight();
        $waste->setDestroy();
        $waste->setCO2emitted($this->getCO2emitted($waste));
    }

}