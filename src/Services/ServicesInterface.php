<?php

namespace App\Services;
use App\Entity\Waste;

interface ServicesInterface{

    function canHandle(Waste $waste): bool;
    function handle(Waste $waste): void;
    function getCO2emitted(): float;
    function getCO2AfterSorting(Waste $waste) : float;

}
