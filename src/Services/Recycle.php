<?php

namespace App\Services;

use App\Entity\Waste;

class Recycle implements ServicesInterface{

    // public function getRecyclingCapacity()
    // {
    //     $json = file_get_contents('./data.json');
    //     $parsed_json = json_decode($json);
    //     $total = 0;
    //     for ($index=0; $index < 10; $index++) {
    //         $services = $parsed_json->{'services'}[$index];
    //         $centers = $parsed_json->{'services'}[$index]->{'type'};
    //         if ($centers === 'recyclagePlastique') {
    //             $capacity = $services->{'capacite'};
    //             $total += $capacity;
    //             echo $capacity . "  "; 
    //         }
    //     }
    // }
   
    private int $capacityMax;
    private string $type;
    private array $subType;
    private int $wasteHandled = 0;
    private float $CO2emitted = 0;
    private array $emission;

    function __construct(int $capacityMax, string $type, array $subType = NULL, ?bool $deposit = NULL, array $emission)
    {
        $this->capacityMax = $capacityMax;
        $this->type = $type;
        $this->subType = $subType;
        $this->deposit = $deposit;
        $this->emission = $emission;

        $this->Recycable();
    }

    // handle the recycling 
    public function handle(Waste $waste): void
    {
        $this->capacityMax -= $waste->getWeight();
        $this->wasteHandled++;
        $waste->setDestroy();
        $waste->setCO2emitted($this->getCO2AfterSorting($waste));
        $waste->setProcessWasteType(str_replace('Handler', '', explode('\\', get_class($this))[2]));
    }
    // Fetching the Waste of type 'recycable'
    private function Recycable(): void
    {
        $this->type = strtolower(str_replace("recyclage", "", $this->type));
    }

    public function getCO2AfterSorting(Waste $waste): float
    {
        if ($waste->getSubType() !== null) {

            return current(array_column($this->emission[$waste->getType()], current([$waste->getSubType()]))) / 1000 * $waste->getweight();
        } else {
            return $this->emission[$waste->getType()] / 1000 * $waste->getweight();
        }
    }

    public function getCO2emitted(): float
    {
        return $this->CO2emitted;
    }

    // Depending on the waste we see if the sorting is doable
    public function canHandle(Waste $waste): bool
    {
        if ($this->type == $waste->getType()) {

            if ($waste->getSubType() && $waste->getSubType() !== "") {
                for ($i = 0; $i < count($this->subType); $i++) {

                    if ($this->subType[$i] == $waste->getSubType()) {
                        if ($this->capacityMax >= $waste->getWeight()) {
                            return true;
                        }
                        return false;
                    }
                }
                return false;
            } else {

                if ($this->capacityMax >= $waste->getWeight()) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }
}
