<?php

namespace App\Services;

use App\Entity\Waste;

class SortingCenter {

    private $capacityMax;

    function __construct(int $capacityMax)
    {
        $this->capacityMax = $capacityMax;
    }

    private function fetchWeakCO2Handler(Waste $waste, array $wasteHandlers) : int
    {
        $treatedCO2 = 999;
        $indexHandler = 0;

        for ($index=0; $index < count($wasteHandlers) ; $index++) { 
            
            if($wasteHandlers[$index]->canHandle($waste))
            {
                if( $wasteHandlers[$index]->getCO2emitted($waste) < $treatedCO2 )
                {
                    $treatedCO2 = $wasteHandlers[$index]->getCO2emitted($waste);
                    $indexHandler = $index;
                }
            }
        }

        return $indexHandler;
    }

    public function canHandle(Waste $waste) : bool
    {
        if( $this->capacityMax >= $waste->getWeight() )
        {
            return true;
        }
        return false;
    }

    public function handle(Waste $waste, array $wastesHandlers) : void 
    {

        $handlersIterator = $this->fetchWeakCO2Handler($waste, $wastesHandlers);
        $wastesHandlers[$handlersIterator]->handle($waste);
        $this->capacityMax -= $waste->getWeight();

    }
}
