<?php

namespace App\Services;

use App\Entity\Waste;

class Composter implements ServicesInterface{
    
    private $capacityMax;
    private int $emission;
    private float $CO2emitted = 0;


    function __construct(int $capacityMax, int $emission)
    {
        $this->capacityMax = $capacityMax;
        $this->emission = $emission;
    }

    public function getCO2AfterSorting(Waste $waste): float
    {
        return $this->emission[$waste->getType()] / 1000 * $waste->getWeight();
    }



    public function getCO2emitted() : float
    {
        return $this->CO2emitted;
    }

    public function canHandle(Waste $waste) : bool
    {
        if( $this->capacityMax >= $waste->getWeight() )
        {
            return true;
        }
        return false;
    }

    public function handle(Waste $waste) : void 
    {
        $this->capacityMax -= $waste->getWeight();
        $waste->setDestroy();
        $waste->setCO2emitted($this->getCO2AfterSorting($waste));
        $waste->setProcessWasteType(str_replace('Handler', '', explode('\\', get_class($this))[2]));
    }
}